Collin Varisco
INFX 210
C00304484

# <u>The Task</u>
#### Manage credentials and account settings in the provided software. Finish by completing all of the objectives.

## <u>The Provided Software</u>
#### CryptoVault, a GUI password manager, is software that I created and stopped working on a little over a year ago. The software offers the typical password management features as well as a feature to securely share selected credentials with other people. The software is fully functional, but its UI design could be improved.

## <u>Objectives</u>:
1. Account Creation
2. Add multiple credentials.
3. Copy a value from a credential's cell and paste is elsewhere withing the program.
4. Use the password generator to generate a password for a credential.
6. Export at least one credential, but not all credentials.
7. Remove a credential
8. Reset Password
9. Set and activate the Automatic Logout setting that automatically logs out of the program when the user has been inactive for a specified amount of time.


---
# <h1 style="border: 5px solid; text-align: center;">Observations</h1>

# User #1: Seth

## <u>Software Experience</u>
- Above average software experience. 
- Has used a wide variety of desktop software.
- Has an I.T. certification

## <u>Steps Taken to Complete Objectives</u>
1. Clicked Create Account
2. Filled out account login credentials and advanced to the main menu.
3. Clicked the "Add Credential" button
4. Filled the form and clicked the "Add" button.
5. Repeated steps 3-4 two more times.
6. Navigated to the settings/features tab.
7. Used the software's password generator to generate a more secure password for a credential. 
8. Copied the generated password.
9. Navigated back to the vault/main menu.
10. Selected a credential, clicked the "Edit Credential" button, and pasted the password in the "Edit Credential" form.
11. Selected the checkbox of one credentials.
12. Hovered over the Import/Export tab to pop out features and clicked on the "Export Selected" option. 
13. Entered a different username and password that someone else would have.
14. Selected a directory to export the encrypted file.
15. Removed a credential
16. Reset password.
17. Set and Turned on the Automatic Logout feature

## <u>Successes and Breakdowns</u>

### <font style="color: #32CD32;">Success</font>: Account Creation![[20230223_170228.jpg]]
> Seth quickly creating an account.


### <font style="color: #32CD32;">Success</font>: Creating a Credential
![[20230223_170400.jpg]]
> This shows Seth filling out the form to add a credential after quickly locating the "Add Credential" button.

### <font style="color: red;">Failure</font>: Locating the Password Generator
![[20230223_170905.jpg]]
> This photo shows Seth looking at the import/export options after looking everywhere else on the screen and right clicking in the area of the credentials to see if it would bring up a submenu with a password generator option. Afterwards he found it in the settings menu and used it successfully.

### <font style="color: #32CD32;">Success</font>: Exporting a Credential
![[20230223_171045.jpg]]
> This photo was taken after Seth had selected one credential to be exported and had pressed the "Export Selected" option from the import/export pop-out options. From here, he went on to save the encrypted credentials file in the operating system's default Documents directory.

### <font style="color: red;">Failure</font>:  Uncertain recognition of the Automatic logout feature
![[20230223_171208.jpg]]
> When searching for the password generator, Seth noticed the feature but stared at it for longer than he should have because the label was not completely visible.
<br>
# User #2: Victor

## <u>Software Experience</u>
- Average software experience. 
- Uses Microsoft Office software regularly.
- Uses other common desktop applications.

## <u>Steps Taken to Complete Objectives</u>
1. Clicked Create Account
2. Filled out account login credentials and advanced to the main menu.
3. Clicked the "Add Credential" button
4. Filled the form and clicked the "Add" button.
5. Repeated steps 3-4 two more times.
6. Navigated to the settings/features tab.
7. Set and Turned on the Automatic Logout feature
8. Reset Password
9. Used the software's password generator to generate a more secure password for a credential. 
10. Copied the generated password.
11. Navigated back to the vault/main menu.
12. Clicked on one of the three created credentials.
13. Clicked the "Edit Credential" button.
14. Pasted the password in the "Edit Credential" form and saved the changes.
15. Selected the checkbox of two of the credentials.
16. Hovered over the Import/Export tab to pop out features and clicked on the "Export Selected" option. 
17. Entered a different username and password that someone else would have.
18. Selected a directory to export the encrypted file.
19. Removed a credential

## <u>Successes and Breakdowns</u>
### <font style="color: #32CD32;">Success</font>: Account Creation
![[20230223_191149.jpg]]
> Victor quickly creating an account.

### <u style="color: #32CD32;">Success</u>: Adding a credential.
![[20230223_191341.jpg]]
> Victor quickly finding the "Add Credential" button and filling out the form to create a credential.

### <font style="color: red;">Failure</font>:  Uncertain recognition of the Automatic logout feature
![[20230223_191427.jpg]]
> Victor spends longer than he should have to when identifying the automatic logout feature on the screen.

### <font style="color: #32CD32;">Success</font>: Use of the Export Selected Feature
![[20230223_191648.jpg]]
> Victor having already selected two credentials to export and is about to press "Export Selected" button.

### <u style="color: red;">Failure</u>: Removal of a credential
![[20230223_191813.jpg]]
> Victor staring at the screen for longer than he should have to when making sure that he has the correct credential selected for removal.


<br><hr><br>

# User #3: Wanda
## <u>Software Experience</u>
- Below average software experience
- Almost all of her time spent on computers has been through browsers. (Browsing internet or Email)
- Has trouble navigating user interfaces she is not familiar with.

## <u>Steps Taken to Complete Objectives</u>
1. Clicked Create Account
2. Filled out account login credentials and advanced to the main menu.
3. Clicked the "Add Credential" button
4. Filled the form and clicked the "Add" button.
5. Created one more credential.
6. Navigated to the settings/features tab.
7. Used the software's password generator to generate a more secure password for a credential. 
8. Copied the generated password.
9. Navigated back to the vault/main menu.
10. Clicked on "Add Credential" button.
11. Pasted the password in the "Add Credential" form when creating the new credential.
12. Selected the checkbox of one of the credentials.
13. Hovered over the Import/Export tab to pop out features and clicked on the "Export Selected" option. 
14. Entered a different username and password that someone else would have.
15. Selected a directory to export the encrypted file.
16. Removed a credential.
17. Reset Password.
18. Set and turned on the automatic logout feature.

## <u>Successes and Breakdowns</u>

### <font style="color: #32CD32;">Success</font>: Account Creation
![[20230223_165202.jpg]]
> Wanda quickly creating an account.

### <font style="color: #32CD32;">Success</font>: Adding Credentials
![[20230223_165250.jpg]]
> Wanda having no difficulty adding a credential.

### <font style="color: red;">Failure</font>: Locating the settings tab
![[20230223_165418.jpg]]
> She had trouble locating any of the options in the settings menu because the settings button in the left panel is not labeled and she did not know the gear icon symbolized that.

### <u style="color: red;">Failure</u>: Password reset
![[20230223_165504.jpg]]
> Due to there not being a second password textbox to confirm that she typed the password that she intended to type, she is typing very slowly to make sure that she does not make a mistake.

### <font style="color: red;">Failure</font>: Uncertain recognition of the automatic logout feature in the settings tab.
![[20230223_165550.jpg]]
> Wanda is unable to recognize the automatic logout feature in the settings menu within a reasonable amount of time.

### <font style="color: #32CD32;">Success</font>: Use of the automatic logout feature
![[20230223_165633.jpg]]
> After asking what the complete label says, she was quickly able to set the automatic timeout duration to 1 minute without activity and enable it.
<br><hr><br>
# <u>User Needs</u>:

## All Users:
1. All users need improved readability of the credentials in row-column format. Specifically to differentiate the data from row to row.
2. All users were somewhat confused when reading the label for the logout timer option in the settings tab. This is because of a UI bug that causes most of the label to be hidden. It shows "Automatically lo ___ (minutes/hours) of inactivity." instead of "Automatically logout after ___ (minutes/hours) of inactivity.". The users need to be able to read all of the label's text to easily identify the feature.

## User #1: Seth
1. Seth needs to be able to generate passwords without leaving the Vault/Main Menu.
2. Seth needs to be able to select all "Export" checkboxes so he only needs to deselect a few instead of selecting the majority.

## User #2: Victor
1. Victor is concerned that he may accidentally delete the wrong credential by misclicking when selecting a credential. Especially because there is no confirmation prompt or recycling bin. Victor needs a way to be sure that he is deleting the intended credential.
2. Victor has a privacy concern regarding the passwords being visible in cleartext on the main menu of the software. Victor needs his passwords to remain hidden on the screen.

## User #3: Wanda
1. Wanda needs to be able to easily see and identify the icons on left panel.	
2. When resetting the password, the password types out as censored dots and does not have a confirmation textbox. Wanda needs a way to verify that she typed what she intended to.




---



